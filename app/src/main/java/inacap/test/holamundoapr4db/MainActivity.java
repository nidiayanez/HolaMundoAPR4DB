package inacap.test.holamundoapr4db;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import inacap.test.holamundoapr4db.vista.FormularioActivity;

public class MainActivity extends AppCompatActivity {

    private EditText editTextUsername, etPassword;
    private Button btLogin;
    private TextView tvPassword, tvRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.editTextUsername = (EditText) findViewById(R.id.etUsername);
        this.etPassword = (EditText) findViewById(R.id.etPassword);

        this.btLogin = (Button) findViewById(R.id.btLogin);
        this.tvPassword = (TextView) findViewById(R.id.tvPassword);
        this.tvRegister = (TextView) findViewById(R.id.tvRegister);

        // Esperar a que el usuario haga 'click' en el Button
        this.btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Obtener contraseña
                String password = etPassword.getText().toString();

                // Mostrar variable
                tvPassword.setText(password);

                // Mostrar Toast (mensaje temporal)
                Toast.makeText(getApplicationContext(), "Password: " + password, Toast.LENGTH_SHORT).show();

            }
        });

        this.tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // INiciar la segunda activity
                Intent nuevaVentana = new Intent(MainActivity.this, FormularioActivity.class);
                startActivity(nuevaVentana);

            }
        });

    }
}
